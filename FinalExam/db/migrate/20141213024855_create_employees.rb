class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.integer :age
      t.string :first_name
      t.string :last_name
      t.float :salary
      t.integer :address_id

      t.timestamps
    end
  end
end
