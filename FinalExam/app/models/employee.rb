class Employee < ActiveRecord::Base

  belongs_to :address

  def withholding(salary)
    return salary*0.1
  end
end
