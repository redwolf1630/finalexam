json.array!(@employees) do |employee|
  json.extract! employee, :id, :age, :first_name, :last_name, :salary, :address_id
  json.url employee_url(employee, format: :json)
end
